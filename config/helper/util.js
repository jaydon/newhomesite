/**
 * Created by leepc on 9/30/2016.
 */
exports.parseCookies = function(request) {
    var list = {},
        rc = request.headers.cookie;

    rc.split(';').forEach(function(cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = unescape(parts.join('='));
    });

    return list;
};