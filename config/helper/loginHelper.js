/**
 * Copyright 2014 Erealm Info & Tech.
 *
 * Created by ken on 06/03/2015.
 */

'use strict';

var config = require('../config'),
    request = require('request'),
    _ = require('lodash');

exports.login = function (req, res, next) {
    var loginBody = {
        client_id: config.clientId,
        client_secret: config.clientSecret
    };

    _.extend(loginBody, req.body);

    var loginOption = {
        'method': req.method,
        'uri': config.adminHost + '/oauth/token',
        'json': loginBody
    };

    request(loginOption, function (error, response, result) {
        if (error) {
            config.error('user login or refresh token error' + error);
            config.error('loginOption', loginOption);
            return res.status(500).send({code: 500});
        } else {
            config.info('request auth success!');
            return res.send(result);
        }
    });
};
