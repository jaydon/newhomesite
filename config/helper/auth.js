/**
 * Copyright 2014 Erealm Info & Tech.
 *
 * Created by ken on 06/03/2015.
 */

'use strict';

var passport = require('passport'),
	path = require('path'),
	config = require('../config'),
	request = require('request'),
	responseHandler = require(path.resolve('./config/helper/responseHelper'));

exports.isAllowed = function (req, res, next) {
	var policyOption = {
		'method': 'POST',
		'uri': config.adminHost + '/controllers/base/policy',
		'headers': req.headers,
		'json': {
			policyName: 'sit-web',
			'path': req.route.path,
			'method': req.method.toLowerCase()
		}
	};

	request(policyOption, function (error, response, result) {
		if (error) {
			config.error('check permission error:' + error);
			return res.status(500);
		}
		if (result.data && result.data.isAllowed) {
			config.info('check permission success');
			return next();
		} else {
			return res.status(403).send(responseHandler.getResponseData(403));
		}
	});
};
