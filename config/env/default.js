'use strict';
var winston = require('winston'),
    path = require('path');

module.exports = {
    app: {
        version: '1.0.0',
        name: process.env.APP_NAME || 'commonWeb',
        title: process.env.APP_TITLE || 'commonWeb',
        description: process.env.APP_DESCRIPTION || 'commonWeb',
        keywords: process.env.APP_KEYWORDS || 'commonWeb',
        googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID'
    },
    fileOptions: {
        maxFieldsSize: 512 * 1024, // 500K,
    },
    port: process.env.PORT || 80,
    host: process.env.HOST || '0.0.0.0',
    adminHost: process.env.ADMIN_HOST||'http://service.aceconsulting.cn',
    clientId: process.env.CLIENT_ID||'123gogogo',
    clientSecret: process.env.CLIENT_SECRET||'3916e3ce9425c5f5805a227295d4043038b7bdaf',
    db: {
        uri: process.env.DB_CONFIG_URI || "mongodb://user:123gogogo@120.27.52.242:27758/common_service-dev",
        options: {
            user: '',
            pass: ''
        },
        // Enable mongoose debug mode
        debug: process.env.MONGODB_DEBUG || false
    },
    templateEngine: 'swig',
    // Session Cookie settings
    sessionCookie: {
        // session expiration is set by default to 24 hours
        maxAge: 24 * (60 * 60 * 1000),
        // httpOnly flag makes sure the cookie is only accessed
        // through the HTTP protocol and not JS/browser
        httpOnly: true,
        // secure cookie should be turned to true to provide additional
        // layer of security so that the cookie is set only when working
        // in HTTPS mode.
        secure: false
    },
    // sessionSecret should be changed for security measures and concerns
    sessionSecret: process.env.SESSION_SECRET || 'MEAN',
    // sessionKey is set to the generic sessionId key used by PHP applications
    // for obsecurity reasons
    sessionKey: 'sessionId',
    sessionCollection: 'sessions',
    mail: {
        enable: true,
        fromaddress: 'eRealm Info & Tech',
        options: {
            service: 'Gmail',
            auth: {
                user: 'lk448075443@gmail.com',//process.env.GMAIL_USER,
                pass: 'jingfengaixue123'//process.env.GMAIL_PASS
            }
        }
    },
    logger: {
        transports: [
            new winston.transports.Console({
                colorize: true
            })
        ]
    },
    logo: './public/img/logo.png',
    favicon: './public/img/favicon.ico',
    uploads: {
        profileUpload: {
            dest: './modules/users/client/img/profile/uploads/', // Profile upload destination path
            limits: {
                fileSize: 1 * 1024 * 1024 // Max file size in bytes (1 MB)
            }
        }
    },
    livereload: true
};
