(function() {
    'use strict';

    angular
        .module('core')
        .factory('errorHttpInterceptor', errorHttpInterceptor);

    errorHttpInterceptor.$inject = ['$q', '$rootScope', '$injector', 'localStorageService'];

    function errorHttpInterceptor($q, $rootScope, $injector, localStorageService) {
        $rootScope.mainLoading = false;
        $rootScope.http = null;

        return {
            'request': function(config) {
                config.headers = config.headers || {};
                if (!config.headers['background']) {
                    $rootScope.mainLoading = true;
                }
                var authData = localStorageService.get('authorizationData');
                if (authData) {
                    config.headers.Authorization = 'Bearer ' + authData.token;
                }
                return config || $q.when(config);
            },
            'requestError': function(rejection) {
                $rootScope.http = $rootScope.http || $injector.get('$http');
                if ($rootScope.http.pendingRequests.length < 1) {
                    $rootScope.mainLoading = false;
                }

                return $q.reject(rejection);
            },
            'response': function(response) {
                $rootScope.http = $rootScope.http || $injector.get('$http');
                if ($rootScope.http.pendingRequests.length < 1) {
                    $rootScope.mainLoading = false;
                }

                if (response.data.code) {
                    if (response.data.code !== 'success' && response.data.message) {
                        $rootScope.showMessage(response.data);
                    }
                    response.data = response.data.data;
                }

                return response || $q.when(response);
            },
            'responseError': function(rejection) {
                $rootScope.http = $rootScope.http || $injector.get('$http');
                if ($rootScope.http.pendingRequests.length < 1) {
                    $rootScope.mainLoading = false;
                }
                if (rejection.status === 500) $rootScope.showMessage(rejection.status + '');
                return $q.reject(rejection);
            }
        };
    }
})();
