(function (app) {
    'use strict';

    app.registerModule('core');
    app.registerModule('core.routes', ['ui.router', 'ui.bootstrap']);
    app.registerModule('core.controller');
}(ApplicationConfiguration));
