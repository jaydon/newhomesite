/**
 *Copyright 2014 Erealm Info & Tech.
 *
 * Created by ken on 07/23/2015.
 */

(function () {
	'use strict';

	angular
		.module('users')
		.controller('changePasswordController', changePasswordController);

	changePasswordController.$inject = ['$rootScope', '$scope', 'AccountService', 'Authentication'];

	function changePasswordController($rootScope, $scope, AccountService, Authentication) {
		$scope.passwords = {};

		var user = Authentication.user;
		$scope.changePassword = function () { 
			AccountService.changePassword(user._id, $scope.passwords);
		};
		$scope.reset = function () {  
			$scope.changePwdForm.$setPristine();
			$scope.passwords = {
				'password': '',
				'newPassword': '',
				'confirmPassword': ''
			};
		};
	}
}());
