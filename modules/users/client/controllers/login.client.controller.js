(function () {
    'use strict';

    angular
        .module('users')
        .controller('loginController', loginController)
        .controller('SignUpModalController', SignUpModalController);

    loginController.$inject = ['$rootScope','$scope', 'Authentication','$state', 'AccountService', '$stateParams'];

    function loginController($rootScope, $scope, Authentication, $state, AccountService, $stateParams) {
        
        $scope.currentAction = $stateParams.currentAction;
        if(!$scope.currentAction){
            $scope.currentAction = 'login';
        }
        $scope.authentication = Authentication;
        $scope.user = {
            username: 'info@lineagesolutions.com',
            password: 'default'
        };

        $scope.templates = {
            'login': {
                tpl: '/login_form',
                form: '#login_form',
                title: 'User Authentication'
            },
            'signUp': {
                tpl: '/sign_up_form',
                form: '#sign_up_form',
                title: 'Sign Up'
            },
            'first': {
                tpl: '/firstlogin_form',
                form: '#firstlogin_form',
                title: 'Change Password'
            },
            'forgot': {
                tpl: '/forgotpassword_form',
                form: '#forgotpassword_form',
                title: 'Forgot Password'
            }
        };

        $scope.changepass = {};
        $scope.forgotpass = {};
        $scope.resetpass = {};
        $scope.partner = {};

        $scope.changeAction = function (action) {
            $scope.errorMessage = null;
            $scope.user = {};
            $scope.changepass = {};
            $scope.forgotpass = {};
            $scope.partner = {};
            $scope.currentAction = action;
        };

        $scope.submit = function (event) {
            if (event.keyCode === 13) {
                if ($scope.currentAction === 'first') {
                    $scope.changePassword();
                } else if ($scope.currentAction === 'forgot') {
                    $scope.forgotPassword();
                } else if ($scope.currentAction === 'signUp') {
                    $scope.signUpUser();
                } else {
                    $scope.login();
                }
            }
        };
        
        $scope.signUpUser = function () {
            AccountService.createAccount($scope.user).then(function (result) {
                if(result.status === 200){
                    $scope.currentAction = 'login';
                }
            });
        };

        $scope.login = function () {
            Authentication.login($scope.user).then(function (result) {
                Authentication.loginConfirmed();
                
                var user = result;
                if (!user) return;

                $scope.authentication.user = user;
                $rootScope.user = user;

                if (user.status === 0) {
                    $scope.currentAction = 'first';
                } else {
                    $scope.$root.mainLoading = true;
                    $state.go('users');
                }
            });
        }; 

        $scope.changePassword = function () {
            AccountService.changePassword($scope.changepass, $scope.authentication.user._id).then(function (result) {
                if (!result) return;
                $scope.changepass = {};
                $scope.user = {};
                $scope.currentAction = 'login';
            });
        };

        $scope.forgotPassword = function (email) {
            var userInfo = {
                email : email,
                resetUrl : window.location.origin + '/reset-password/'
            };
            AccountService.forgotPassword(userInfo).then(function (result) {
                if (!result) return;
                $scope.forgotPass = {};
                $scope.currentAction = 'login';
            });
        };

        $scope.findPassword = function () {
            AccountService.findPasswordByUser($scope.resetpass).then(function (result) {
                if (!result) return;
                $scope.resetpass = {};
            });
        };
    }

    SignUpModalController.$inject =['$scope', '$state','$uibModalInstance', 'Authentication'];

    function SignUpModalController($scope, $state, $modalInstance, Authentication) {
        $scope.user = {
            username: 'admin@lineagesolutions.com',
            password: 'default'
        };

        $scope.login = function () {
            $modalInstance.close({"user": $scope.user});
        };

        $scope.createAccount = function () {
            $modalInstance.close({"action": "createAccount", "user": $scope.user});
        };

        $scope.resetPassword = function () {
            $modalInstance.close({"user": $scope.user});
        };

        $scope.changeToResetPassword = function() {
            $modalInstance.close({"action":"changeToResetPassword"});
        };

        $scope.changeToCreateAccount = function() {
            $modalInstance.close({"action":"changeToCreateAccount"});
        };

        $scope.changeToSignIn = function() {
            $modalInstance.close({"action":"changeToSignIn", "user": null});
        };

        $scope.inputType = 'password';
        $scope.hideShowPassword = function (){
            if ($scope.inputType == 'password'){
                $scope.inputType = 'text';
            } else {
                $scope.inputType = 'password';
            }

        };
    }
}());
