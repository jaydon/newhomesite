(function () {
    'use strict';

    angular
        .module('users')
        .controller('findPasswordController', findPasswordController);

    findPasswordController.$inject = ['$scope', '$stateParams', 'AccountService'];

    function findPasswordController($scope, $stateParams, AccountService) {

        var findPasswordToken = $stateParams.token;
        AccountService.checkResetToken(findPasswordToken).then(function (result) {
            $scope.isTokenValid = result.data.isTokenValid;
        });
        $scope.changePassword = function (newPassword, confirmPassword) {
            var findPasswordInfo = {
                newPassword: newPassword,
                confirmPassword: confirmPassword,
                token: findPasswordToken  
            };
            AccountService.findPassword(findPasswordInfo);
        }
    }
})();

