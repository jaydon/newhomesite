(function (app) {
    'use strict';

    app.registerModule('erealm',['core','shoppinpal.mobile-menu']);
    app.registerModule('erealm.routes', ['ui.router']);
    app.registerModule('erealm.translate');
    app.registerModule('erealm.service',['ngCookies']);
}(ApplicationConfiguration));