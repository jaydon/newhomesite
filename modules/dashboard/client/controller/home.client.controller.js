/**
 * Created by jaydon lee on 9/19/2016.
 */
(function(){
    angular.module('erealm')
        .controller('homeController',homeController);
    homeController.$inject=['$scope','DashboardService', '$translate', '$interval'];
    function homeController($scope, DashboardService, $translate, $interval){
        'use strict';
        angular.extend($scope,{subTitle: "Hello, We are", mainTitle: "eRealm Info & Tech", currentPage: "home-page"});
        $scope.myInterval = 5000;
        $scope.isRise = false;
        $scope.isRiseCan = false;
        $scope.isRiseMan = false;
        $scope.isSlideLeftFirst = false;
        $scope.isSlideRightFirst = false;
        $scope.isSlideLeftSecond = false;
        $scope.aboutprogress={
            'members':{'name':'Social Media Marketing','value':0,'stop':55},
            'salary':{'name':'Design','value':0,'stop':90},
            'customers':{'name':'Consulting','value':0,'stop':44},
            'projects':{'name':'Bounce Rate','value':0,'stop':62}
        };
        $scope.loadData = function(language) {
            if (!language){
                language = $translate.use();
                console.log(language);
            }
          
            DashboardService.getManagement(language).then(function(response) {
                $scope.roles = response.data.roles;
                $scope.skills = response.data.skills;
                $scope.management = response.data.management;
                console.log( $scope.management);
            });
           
            // $timeout(function(){
            //     DashboardService.checkPosts('cn').then(function(response){
            //         $scope.postsNotification = response.data.result;
            //     });
            // }, 2000);
        };

        $scope.loadData();
        $scope.slides=[
            {"id":0,'image':'/img/background-0.png'},

            {"id":1,'image':'/img/background-6.png'}

        ]
        DashboardService.getTechnologies().then(function(response) {
            console.log(response);
            $scope.technologies = response.data.technologies;
            $scope.languages = response.data.languages;
        });
        // var logoCarousel = document.getElementsByClassName('programming-language');
        // var first = logoCarousel.children[0];
        // var maxl = 0;
        // $interval(function(){
        //     maxl -=0.3;
        //     if(maxl>first.offsetWidth){
        //         first.style.marginLeft = null;
        //         logoCarousel.appendChild(first);
        //         first =logoCarousel.children[0];
        //         maxl = -0.2;
        //     }
        //     first.style.marginLeft = maxl + 'px';
        // },100);
      

        $scope.getTechClass = function(familiarity) {
            return "tech-" + Math.ceil(familiarity/2);
        };
    }
}());