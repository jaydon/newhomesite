/**
 * Created by jaydon on 9/19/2016.
 */
(function () {
    'use strict';
    angular.module('erealm').controller('newsController', newsController);
    newsController.$inject = ['$scope', 'DashboardService', '$translate', '$http','$state'];
    function newsController($scope, DashboardService, $translate, $http,$state) {
        angular.extend($scope, {subTitle: "Hello, We are", mainTitle: "eRealm Info & Tech", currentPage: "blog-page"});
        $scope.loadData = function (language) {
            if (!language) {
                language = $translate.use();
            }
        }
        $scope.loadData();
        $http.get('mock/newsDataTop.json').success(function (responseData) {
            $scope.bannerTopData = responseData;
        });
        $http.get('mock/newsDataRight.json').success(function (responseData) {
            $scope.bannerRightData = responseData;
        });
        $http.get('mock/newsDataLeft.json').success(function (responseData) {
            $scope.bannerLeftData = responseData;
        }).then(function () {
            $scope.pageData = new Object();
            $scope.pageData.currentPage = 1;
            $scope.pageData.perInfoNum = 10;
            $scope.pageData.infoNum = $scope.bannerLeftData.length;
            $scope.pageData.pageNum = Math.ceil($scope.pageData.infoNum / $scope.pageData.perInfoNum);
            $scope.pageData.getPage = function (page) {
                $scope.perPageList = $scope.bannerLeftData.slice((page - 1) * 10, page * 10);
            };
            $scope.pageData.getPage($scope.pageData.currentPage);
        });
    }
}());