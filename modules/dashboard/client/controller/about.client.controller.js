/**
 * Created by lee on 9/19/2016.
 */
(function(){
    angular.module('erealm')
        .controller('aboutController',aboutController);
    aboutController.$inject=['$scope','DashboardService','$translate', '$interval'];
    function aboutController($scope, DashboardService,$translate,$interval){
        angular.extend($scope,{subTitle: "ABOUT", mainTitle: "working with you", currentPage: "about-page"});
        $scope.isCollapsed = true;
        // $scope.homeBack = 'back' + (Math.floor(Math.random() * 3) + 1);

        $scope.loadData = function(language) {
            debugger;
            if (!language){
                language = $translate.use();
            }
            $scope.loadLanguage = language;
            DashboardService.getEmployeeInfo(language).then(function(response){
                $scope.person = response.data;
            });
            DashboardService.getPartnersInfo(language).then(function(response){
                $scope.partner = response.data;
            });
         };
        $scope.loadData();
        DashboardService.getFlickrPhotos().then(function(response){
            var photos = response.data.items;
            angular.forEach(photos, function(item) {
                item.media.m = item.media.m.replace('_m', '_q');
            });
            $scope.photos = photos;
        });

        $scope.collapsed=function(ite)
        {
            $scope.isCollapsed=!( $scope.isCollapsed);
            $scope.item=ite;
        };

        $scope.addcount = function(){
            alert('hello erealm');
        }
    }
}());